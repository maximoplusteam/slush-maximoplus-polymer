var gulp = require('gulp');
var watch=require('gulp-watch');
var replace=require('gulp-replace');

var cprepare = require('gulp-cordova-create');
var vulcanize = require('gulp-vulcanize');
var minifyInline=require('gulp-minify-inline');
var stripComments=require('gulp-strip-comments');



gulp.task('cordovainit',function(){
    gulp.src(['app/**/*'])
	.pipe(replace('../bower_components','bower_components'))
	.pipe(replace('<!--script type="text/javascript" src="cordova.js"></script-->','<script type="text/javascript" src="cordova.js"></script>'))
	.pipe(gulp.dest('./.cordovaapp/www'));
});

gulp.task('cordovacopy',function(){
    watch(['app/**/*'])
    	.pipe(replace('../bower_components','bower_components'))
	.pipe(replace('<!--script type="text/javascript" src="cordova.js"></script-->','<script type="text/javascript" src="cordova.js"></script>'))
	.pipe(gulp.dest('./.cordovaapp/www'));
});

gulp.task('vulcanize', function() {
  return gulp.src('index.html')
         .pipe(vulcanize())
         .pipe(minifyInline())
         .pipe(stripComments())
         .on( "error", function( err ) {
           console.log( err );
         })
         .pipe(gulp.dest('dist/vulcanized'));
});

gulp.task('mobile-production-cord-prepare', ['vulcanize'],
          function(){
            return gulp.src('dist/vulcanized')
                   .pipe(cprepare({dir:".cordovaprod",id:"com.maximoplus.<%= appName %>", name:"<%= appName %>"}));
          });

gulp.task('mobile-production-prepare',['mobile-production-cord-prepare'],
          function(){
            return gulp.src(['bower_components/webcomponentsjs/webcomponents.min.js','bower_components/maximoplus-base-template/main.js'],{base:'./'})
                   .pipe(gulp.dest('.cordovaprod/www'))
          });

gulp.task('mobile-prod',['mobile-production-prepare'], function(){
  return gulp.src('cordova.json')
         .pipe(cordova({cwd:'.cordovaprod'})
               .on("error", function(err){
                 console.log(err);
               }))
});

